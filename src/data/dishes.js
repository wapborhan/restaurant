const DISHES = [
  {
    id: 0,
    name: "Ginger Burger",
    image: "assets/images/zinger-burger.jpg",
    category: "Meal",
    label: "Hot",
    price: "999",
    description: "Unique Dishes",
    comments: [
      {
        id: 0,
        rating: 5,
        comment: "Good Test",
        author: "Borhan Uddin",
        date: "2022-01-16T17:57:28.556094Z",
      },
      {
        id: 1,
        rating: 5,
        comment: "Its Very Amezing.",
        author: "Hridoy",
        date: "2022-01-16T17:57:28.556094Z",
      },
    ],
  },
  {
    id: 1,
    name: "Ckiken Burger",
    image: "assets/images/Chicken-Burger.jpg",
    category: "Chiken",
    label: "Hot",
    price: "650",
    description: "Unique Dishes",
    comments: [
      {
        id: 0,
        rating: 5,
        comment: "Good Test",
        author: "Borhan Uddin",
        date: "2022-01-16T17:57:28.556094Z",
      },
    ],
  },
  {
    id: 3,
    name: "Pizza",
    image: "assets/images/pizza.jpg",
    category: "Chiken",
    label: "Hot",
    price: "1250",
    description: "Unique Pizza",
    comments: [
      {
        id: 0,
        rating: 5,
        comment: "Good Test",
        author: "Borhan Uddin",
        date: "2022-01-21T17:57:28.556094Z",
      },
    ],
  },
];

export default DISHES;
